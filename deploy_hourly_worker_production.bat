@echo off
setlocal
:PROMPT
echo you are about to deploy Phytech-Hourly-Worker and Phytech-Hourly-Worker2 to Production
SET /P AREYOUSURE=Are you sure (Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO END
echo Deploy Start
call mvn clean heroku:deploy -Dheroku.appName=phytech-hr-worker-production
call mvn clean heroku:deploy -Dheroku.appName=phytech-hr-worker2-production
:END
echo Finished
endlocal
