package com.phytech.PhytechHourlyWorker;

import java.util.List;
import java.util.ListIterator;

import com.phytech.model.Measurement;
import com.phytech.utils.Constants;

public abstract class WaterAbstractCalculator {

	/**
	 * Run through waterFlowMeasurements and complete it by 0
	 * 
	 * @param waterFlowMeasurements
	 * @return
	 */
	protected List<Measurement> completeWaterFlow(List<Measurement> waterFlowMeasurements) {

		if (waterFlowMeasurements == null || waterFlowMeasurements.size() < 2) {
			return waterFlowMeasurements;
		}

		ListIterator<Measurement> iter = waterFlowMeasurements.listIterator();
		Measurement prev = iter.next();
		Measurement current = iter.next();

		while (iter.hasNext()) {

			long deltaTime = current.getTimestamp() - prev.getTimestamp();
			if (deltaTime > Constants.TEN_MINUTES_MILLISECONDS) {
				iter.previous();
				int numberOfMissingMeasurements = (int) (deltaTime / Constants.TEN_MINUTES_MILLISECONDS);
				for (int i = 1; i <= numberOfMissingMeasurements; i++) {
					Measurement missingMeasurement = new Measurement();
					missingMeasurement.setTime(prev.getTimestamp() + (long) i * Constants.TEN_MINUTES_MILLISECONDS);
					missingMeasurement.setValue(0.0);
					missingMeasurement.setFormat(0.0);
					missingMeasurement.setStitched(0.0);
					iter.add(missingMeasurement);
					prev = missingMeasurement;
				}
			} else {
				prev = current;
			}
			current = iter.next();
		}

		return waterFlowMeasurements;
	}

}