package com.phytech.PhytechHourlyWorker;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.phytech.database.DBQueriesManagerProjects;
import com.phytech.database.DBQueriesManagerSensors;
import com.phytech.model.Measurement;
import com.phytech.model.ProjectIrrigationSpan;
import com.phytech.model.ProjectMetadata;
import com.phytech.model.Enumerations.SensorKind;
import com.phytech.model.Sensors.Sensor;
import com.phytech.utils.Constants;
import com.phytech.utils.PropertiesManager;

public class SoilMoistureHourlyCalculator extends HourlyCalculator {
	private static final Logger log = LogManager.getLogger(SoilMoistureHourlyCalculator.class);

	@Override
	protected List<SensorKind> getAssociatedSensorKind() {
		List<SensorKind> kinds = new ArrayList<>();
		kinds.add(SensorKind.SOIL_MOISTURE);
		return kinds;
	}

	@Override
	protected List<Measurement> calcGraphValue(Measurement[] measurements, Measurement prevGraphValue) {
		return new ArrayList<Measurement>();
	}

	@Override
	public void calc(ProjectMetadata metadata, Long from, Long to) {

		// if not flood project, nothing to calculate
		if (!metadata.isFlood()) {
			return;
		}

		// gets the relevant sensors list
		List<Sensor> sensorsOfInterest = metadata.getSensors(getAssociatedSensorKind());

		// no SM sensors in project, nothing to calculate
		if (sensorsOfInterest.isEmpty()) {
			return;
		}

		boolean fromTheBeginning = (from == null);
		List<ProjectIrrigationSpan> spansToUpdate = new ArrayList<ProjectIrrigationSpan>();
		// if not defined start timestamp, use the midnight of project activation date
		long actualStart;
		if (!fromTheBeginning) {
			actualStart = from;
		} else {
			actualStart = metadata.getTheFirstMeasurementTS();
		}

		// log.info("Before add lock");
		//
		// // lock given project in DB
		// DBQueriesManager.addWorkerLock(metadata.getProjectId(),
		// PropertiesManager.workerID,
		// CalculationKind.PROJECT_WATER, startTS);

		
		ProjectIrrigationSpan overlapSpan = DBQueriesManagerProjects.getOverlapedSpan(metadata.getProjectId(),
				actualStart);

		if (overlapSpan != null) {
			log.debug("Affected from " + overlapSpan.toString(metadata.getTimezone()));
		} else {
			log.debug("No affected spans");
		}

		if (!fromTheBeginning && overlapSpan != null ) {
			if (!fromTheBeginning && overlapSpan != null) {
				actualStart = overlapSpan.getTimeSpan().getStartDate();
			}
		} 

		if (to==null) {
			to=System.currentTimeMillis();
		}
 


		Map<Integer, List<Measurement>> measurements = DBQueriesManagerSensors
				.getFormattedMeasurements(sensorsOfInterest, actualStart, to);
		int length = (int) ((to - actualStart) / Constants.TEN_MINUTES_MILLISECONDS);

		boolean[] totalFlood = new boolean[length];

		for (Sensor s : sensorsOfInterest) {
			List<Measurement> oneSensorMeasurements = measurements.get(s.getID());
			if (oneSensorMeasurements == null) {
				continue;
			}

			boolean[] oneSensorFlood = calcSensorFlood(oneSensorMeasurements, actualStart, length);
			for (int i = 0; i < length; i++) {
				totalFlood[i] = totalFlood[i] || oneSensorFlood[i];
			}

		}

		spansToUpdate.addAll(splitToSpans(metadata, totalFlood, actualStart));

		if (PropertiesManager.isDebug()) {
			log.info("Irrigation spans:");
			for (ProjectIrrigationSpan is : spansToUpdate) {
				log.info(is.toString(metadata.getTimezone()));
			}
		}

		boolean isWriteAllowed = true;
		// DBQueriesManager.checkWorkerLock(metadata.getProjectId(),
		// CalculationKind.PROJECT_WATER, PropertiesManager.workerID, startTS);

		if (isWriteAllowed) {
			// write calculations to the DB and remove the lock

			DBQueriesManagerProjects.updateIrrigationSpans(metadata.getProjectId(), spansToUpdate);
			// DBQueriesManager.deleteWorkerLock(metadata.getProjectId(),
			// CalculationKind.PROJECT_HOURLY,
			// PropertiesManager.workerID, startTS);
			log.info("Updated " + spansToUpdate.size() + " spans");

		} else {
			log.warn("Can't update project hourly water calculations for project " + metadata.getProjectId()
					+ " since it's already locked");
		}
	}

	private boolean[] calcSensorFlood(List<Measurement> oneSensorMeasurements, long start, int length) {
		boolean[] flood = new boolean[length];
		if (oneSensorMeasurements == null || oneSensorMeasurements.isEmpty()) {
			return flood;
		}

		Iterator<Measurement> iter = oneSensorMeasurements.iterator();
		Measurement current = null;
		Measurement prev = iter.next();

		while (iter.hasNext()) {
			if (current != null) {
				prev = current;
			}
			current = iter.next();
			double deltaMoistureToFlood = 3.0;
			int deltaHours = (int) ((current.getTimestamp() - prev.getTimestamp()) / Constants.ONE_HOUR_MILLISECONDS);

			//the value itself is in %, so if look for 3% increase it's just X2-X1>3
			if (current.getValue() - prev.getValue() > deltaMoistureToFlood * deltaHours) {
				int measurementIndex = (int) ((current.getTimestamp() - start) / Constants.ONE_HOUR_MILLISECONDS);
				flood[measurementIndex] = true;
			}
		}

		return flood;

	}

	private Collection<ProjectIrrigationSpan> splitToSpans(ProjectMetadata metadata, boolean[] totalFlood,
			long startTs) {

		List<ProjectIrrigationSpan> spans = new ArrayList<ProjectIrrigationSpan>();

		for (int i = 0; i < totalFlood.length; i++) {
			if (totalFlood[i]) {
				long floodDate = startTs + (long) (i * Constants.ONE_HOUR_MILLISECONDS);
				ProjectIrrigationSpan span = new ProjectIrrigationSpan(metadata.getProjectId(),
						floodDate - Constants.ONE_HOUR_MILLISECONDS, floodDate);
				span.setClosed(true);
				span.setAmount((float) 1.0);
				span.setUpdated_at(System.currentTimeMillis());
				spans.add(span);

			}
		}

		return spans;
	}

}
