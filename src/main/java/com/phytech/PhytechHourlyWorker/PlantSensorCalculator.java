package com.phytech.PhytechHourlyWorker;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.phytech.database.DBQueriesManagerSensors;
import com.phytech.model.Measurement;
import com.phytech.model.ProjectMetadata;
import com.phytech.model.ValueValidationEntry;
import com.phytech.model.Enumerations.SensorKind;
import com.phytech.model.Enumerations.SensorType;
import com.phytech.model.Sensors.Sensor;
import com.phytech.utils.Constants;
import com.phytech.utils.SensorValidationDataCache;
import com.phytech.utils.Utils;

public class PlantSensorCalculator extends HourlyCalculator {
	private static final Logger log = LogManager.getLogger(PlantSensorCalculator.class);

	@Override
	protected List<SensorKind> getAssociatedSensorKind() {
		List<SensorKind> kinds = new ArrayList<>();
		kinds.add(SensorKind.PLANT);
		return kinds;
	}

	/**
	 * Recalculate hourly data for one specific sensor
	 * 
	 * @param sensorId
	 *            - sensor id
	 * @param metadata
	 *            - metadata of project, this sensor belongs to
	 * @param startdDate
	 *            - start recalculate date
	 * @param endDate
	 *            - end recalculate date (if null, the sensor shall be recalculated
	 *            till now)
	 */
	public void recalculateSensorHourlyData(int sensorId, ProjectMetadata metadata, Long start, Long end) {
		List<Sensor> sensorsOfInterest = new ArrayList<Sensor>();

		Sensor sensor = metadata.getSensor(sensorId);
		if (sensor == null) {
			return;
		}

		sensorsOfInterest.add(sensor);

		innerRecalculateSensors(metadata, start, end, sensorsOfInterest);
	}

	public void recalculateAllSensors(ProjectMetadata metadata, Long start, long end) {
		List<Sensor> sensorsOfInterest = new ArrayList<Sensor>();

		for (Sensor s : metadata.getSensors().values()) {
			if (s.getSensorKind().equals(SensorKind.PLANT)) {
				sensorsOfInterest.add(s);
			}
		}

		innerRecalculateSensors(metadata, start, end, sensorsOfInterest);
	}

	private void innerRecalculateSensors(ProjectMetadata metadata, Long start, long end,
			List<Sensor> sensorsOfInterest) {
		if (sensorsOfInterest.isEmpty()) {
			return;
		}

		long startTS;
		if (start != null) {
			startTS = start - Constants.ONE_HOUR_MILLISECONDS;
		} else {
			startTS = metadata.getTheFirstMeasurementTS();
		}

		Map<Integer, List<Measurement>> allMeasurements = DBQueriesManagerSensors.getMeasurements(sensorsOfInterest,
				startTS, end);

		Map<Integer, List<Measurement>> allMeasurementsStitched = new HashMap<Integer, List<Measurement>>();

		// run through all sensors and stitch them
		for (Integer sensorId : allMeasurements.keySet()) {

			Sensor sensor = metadata.getSensor(sensorId);

			// special treatment for GPVS sensors
			if (sensor.getSensorType() == SensorType.GPVS) {
				if (metadata.isMature()) {
					sensor.setSensorType(SensorType.DE);
				} else {
					sensor.setSensorType(SensorType.SD_NEW);
				}
			}
			ValueValidationEntry vve = SensorValidationDataCache.getInstance()
					.getValidationEntry(sensor.getSensorType(), sensor.getSensorTypeId());

			if (vve == null) {
				log.warn("Can't found validation params. No plant calc available");
				continue;
			}

			// add zero measurement to the beginning of measurements list, so the system
			// stitch all to 1000
			List<Measurement> oneSensorMeasurements = allMeasurements.get(sensorId);
			if (start == null) {
				oneSensorMeasurements.add(0,
						new Measurement(oneSensorMeasurements.get(0).getTimestamp() - Constants.ONE_HOUR_MILLISECONDS,
								1000.0, 1000.0, 1000.0));
			}
			List<Measurement> stitched = stitchSensorMeasurements(oneSensorMeasurements, metadata,
					vve.getMaxDeltaForStitch(), true);

			// remove the anchor
			stitched.remove(0);

			allMeasurementsStitched.put(sensorId, stitched);

		}

		// save stitched measurements into DB
		DBQueriesManagerSensors.insertSensorsMeasurements(allMeasurementsStitched);
	}

	private List<Measurement> stitchSensorMeasurements(List<Measurement> measurements, ProjectMetadata metadata,
			Double maxToStitch, boolean force) {
		try {
			if (measurements.isEmpty()) {
				return null;
			}
			Measurement last = measurements.get(measurements.size() - 1);

			if ((last != null && last.getStitched() == null) || force) {

				List<Measurement> stitched = Utils.stitchMeasurements(measurements.get(0), measurements, maxToStitch);

				// if (PropertiesManager.isDebug(metadata.getProjectId())) {
				// for (int i = 0; i < stitched.size(); i++) {
				// log.info(Utils.getDateTimeString(measurements.get(i).getTimestamp(),
				// metadata.getTimezone())
				// + "," + stitched.get(i).getValue() + "," + stitched.get(i).getStitched());
				// }
				// }

				return stitched;
			}

		} catch (Exception e) {
			log.error(e);
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			String sStackTrace = sw.toString(); // stack trace as a string
			log.error(sStackTrace);
		}
		return null;
	}

	@Override
	public void calc(ProjectMetadata metadata, Long start, Long end) {
		// TODO Auto-generated method stub

	}

	@Override
	protected List<Measurement> calcGraphValue(Measurement[] measurements, Measurement prevGraphValue) {
		// TODO Auto-generated method stub
		return null;
	}
}
