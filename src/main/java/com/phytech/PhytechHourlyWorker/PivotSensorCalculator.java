package com.phytech.PhytechHourlyWorker;

import java.util.List;

import com.phytech.database.DBQueriesManagerSensors;
import com.phytech.model.BlueLine;
import com.phytech.model.ProjectMetadata;
import com.phytech.model.SensorLocation;
import com.phytech.model.Enumerations.Direction;
import com.phytech.utils.MathUtils;

public class PivotSensorCalculator {

	public void updateBlueLine(ProjectMetadata metadata, int sensorId) {
		Direction currentDirection = null;
		List<SensorLocation> lastIrrigationLocations = DBQueriesManagerSensors.getlastIrrigationLocations(sensorId);
		if (lastIrrigationLocations.isEmpty()) {
			lastIrrigationLocations = DBQueriesManagerSensors.getAllIrrigationLocations(sensorId);
		}
		BlueLine bLineObject = new BlueLine(metadata.getPlotId());
		if (lastIrrigationLocations.size() > 2) {
			 currentDirection = MathUtils.getDirection(lastIrrigationLocations.get(0),
					lastIrrigationLocations.get(1),currentDirection);

			bLineObject.setDirection(currentDirection);
			bLineObject.setStart(lastIrrigationLocations.get(1));
			bLineObject.setFinish(lastIrrigationLocations.get(1));
			bLineObject.setLength(0);
			for (int i = 2; i < lastIrrigationLocations.size(); i++) {
				currentDirection = MathUtils.getDirection(lastIrrigationLocations.get(i - 1),
						lastIrrigationLocations.get(i),currentDirection);
				if (currentDirection.equals(bLineObject.getDirection())) {
					bLineObject.setFinish(lastIrrigationLocations.get(i));

					float actualAngle = lastIrrigationLocations.get(i).getAngle();
					if (currentDirection.equals(Direction.CCW)) {
						if (actualAngle > lastIrrigationLocations.get(i - 1).getAngle()) {
							actualAngle -= 360;
						}
					} else {
						if (actualAngle < lastIrrigationLocations.get(i - 1).getAngle()) {
							actualAngle += 360;

						}
					}
					if (currentDirection.equals(Direction.CCW)) {
						bLineObject.setLength(bLineObject.getLength()
								+ (lastIrrigationLocations.get(i - 1).getAngle() - actualAngle));
					} else {
						bLineObject.setLength(bLineObject.getLength()
								+ (actualAngle - lastIrrigationLocations.get(i - 1).getAngle()));

					}
				} else {
					bLineObject.setDirection(currentDirection);
					bLineObject.setStart(bLineObject.getFinish());
					bLineObject.setFinish(lastIrrigationLocations.get(i));
					bLineObject.setLength(bLineObject.getFinish().getAngle() - bLineObject.getStart().getAngle());
				}
				if (bLineObject.getLength() >= 360) {
					bLineObject.setStart(bLineObject.getFinish());
					bLineObject.setLength(0);
				}
			}

		} else {

			bLineObject.setDirection(Direction.CW);
			bLineObject.setStart(null);
			bLineObject.setLength(0);
			if (lastIrrigationLocations.isEmpty()) {
				bLineObject.setFinish(null);
			} else {
				bLineObject.setFinish(lastIrrigationLocations.get(0));
			}

		}
		// Update the DB
		DBQueriesManagerSensors.updateBlueLine(metadata, sensorId, bLineObject);

	}
}
