package com.phytech.PhytechHourlyWorker;

import java.util.ArrayList;
import java.util.List;

import com.phytech.database.DBQueriesManagerSensors;
import com.phytech.model.Measurement;
import com.phytech.model.ProjectMetadata;
import com.phytech.model.Enumerations.SensorKind;
import com.phytech.utils.Constants;
import com.phytech.utils.PhytechApi;
import com.phytech.utils.TimeUtils;

public class FruitCalculator extends HourlyCalculator {

	@Override
	protected boolean isValidMeasurement(Measurement m) {
		return m != null && m.getFormat() > 20;
	}

	@Override
	protected List<Measurement> calcGraphValue(Measurement[] measurements, Measurement prevGraphValue) {

		List<Measurement> res = new ArrayList<Measurement>();
		Double sum = 0.0;
		int validSensorsCounter = 0;

		Double averageValidDeltaSum = 0.0;
		int averageValidDeltaCounter = 0;

		Measurement currentAverage;
		long currentTs = 0;
		
		boolean [] isSensorValid = new boolean[measurements.length];

		for (int sensorIndx = 0; sensorIndx < measurements.length; sensorIndx++) {
			// first - look for valid measurements
			if (isValidMeasurement(measurements[sensorIndx])) {
					// try to calculate the delta from the last valid measurement of this sensor
				if (prevValid[sensorIndx] != null) {
					currentTs = measurements[sensorIndx].getTimestamp();
					double delta = measurements[sensorIndx].getFormat() - prevValid[sensorIndx].getFormat();
					long deltaTS = currentTs - prevValid[sensorIndx].getTimestamp();
					int deltaHours = (int) (deltaTS / Constants.ONE_HOUR_MILLISECONDS);
					double oneHourDelta = delta / deltaHours;

					// no more than 3 days from last valid and no more than 0.5 mm in a hour change
					if (deltaHours < 72 && ((oneHourDelta >= 0 && oneHourDelta < 2)
							|| (oneHourDelta <= 0 && oneHourDelta > -2))) {
						// the delta is also valid and can be used to complete missing data
						averageValidDeltaSum += oneHourDelta;
						averageValidDeltaCounter++;
						sum += measurements[sensorIndx].getFormat();
						validSensorsCounter++;
						
						prevValid[sensorIndx] = measurements[sensorIndx];
						isSensorValid[sensorIndx]=true;
					}
				}

			}
		}

		// no valid sensors at all. nothing to do
		if (validSensorsCounter == 0) {
			return res;
		}

		currentAverage = new Measurement(currentTs, 0.0);

		if (averageValidDeltaCounter == 0) {
			currentAverage.setFormat(sum / validSensorsCounter);
			currentAverage.setStitched(sum / validSensorsCounter);
			res.add(currentAverage);
			return res;
		}

		double averageDelta = averageValidDeltaSum / averageValidDeltaCounter;

		// some sensors are invalid, try to complete if possible
		if (averageValidDeltaCounter != measurements.length) {
			for (int sensorIndx = 0; sensorIndx < measurements.length; sensorIndx++) {
				if (!isSensorValid[sensorIndx]) {

					// current is invalid, but exist some previous valid for this sensor
					if (prevValid[sensorIndx] != null) {

						long deltaTS = currentTs - prevValid[sensorIndx].getTimestamp();
						int deltaHours = (int) (deltaTS / Constants.ONE_HOUR_MILLISECONDS);

						if (deltaHours < 72) {
							// Allowed to complete this sensor
							sum += prevValid[sensorIndx].getFormat() + averageDelta * deltaHours;
							validSensorsCounter++;
						}
					}
				}
			}

		}

		// current value is invalid
		if (Double.isNaN(sum)) {
			return res;
		} else {
			currentAverage.setFormat(sum / validSensorsCounter);
			currentAverage.setStitched(sum / validSensorsCounter);
		}

		// test for gaps existence, and try to complete
		if (prevGraphValue != null) {
			long deltaTS = currentTs - prevGraphValue.getTimestamp();
			double deltaValue = currentAverage.getFormat() - prevGraphValue.getFormat();

			int deltaHours = (int) (deltaTS / Constants.ONE_HOUR_MILLISECONDS);

			double oneHourDelta = deltaValue / deltaHours;
			if (deltaHours > 1) {
				for (int i = 1; i <= deltaHours; i++) {
					Measurement demo = new Measurement(
							prevGraphValue.getTimestamp() + i * Constants.ONE_HOUR_MILLISECONDS, 0.0);

					double value = prevGraphValue.getFormat() + i * oneHourDelta;
					demo.setFormat(value);
					demo.setStitched(value);
					res.add(demo);
				}
			}
		}

		res.add(currentAverage);

		return res;
	}

	@Override
	protected void initPrevValid(ProjectMetadata metadata, List<Measurement[]> measurementsByTS, Long start, Long ts) {

		prevValid = DBQueriesManagerSensors.getPrevValid(
				PhytechApi.getProjectIgnoredDates(metadata, getAssociatedSensorKind()),
				TimeUtils.moveDateBackward(ts, 7, metadata.getTimezone()), ts);

	}

	@Override
	protected List<SensorKind> getAssociatedSensorKind() {
		List<SensorKind> kinds = new ArrayList<>();
		kinds.add(SensorKind.FRUIT);
		return kinds;
	}

}
