package com.phytech.PhytechHourlyWorker;

import java.util.ArrayList;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.phytech.database.DBQueriesManager;
import com.phytech.database.DBQueriesManagerProjects;
import com.phytech.database.DBQueriesManagerSensors;
import com.phytech.model.IrrigationData;
import com.phytech.model.Measurement;
import com.phytech.model.ProjectHourlyLock;
import com.phytech.model.ProjectIrrigationSpan;
import com.phytech.model.ProjectMetadata;
import com.phytech.model.SensorLocation;
import com.phytech.model.Enumerations.Direction;
import com.phytech.model.Enumerations.SensorKind;
import com.phytech.model.Sensors.Sensor;
import com.phytech.utils.Constants;
import com.phytech.utils.MathUtils;
import com.phytech.utils.PhytechApi;
import com.phytech.utils.PropertiesManager;
import com.phytech.utils.TimeUtils;
import com.phytech.utils.Utils;

public class PivotProjectHourlyCalculator extends HourlyCalculator {
	private static final long timeResolution = Constants.ONE_HOUR_MILLISECONDS;

	private static final Logger log = LogManager.getLogger(PivotProjectHourlyCalculator.class);

	@Override
	protected List<SensorKind> getAssociatedSensorKind() {
		List<SensorKind> kinds = new ArrayList<>();
		kinds.add(SensorKind.PIVOT);
		return kinds;
	}

	@Override
	public void calc(ProjectMetadata metadata, Long from, Long to) {

		boolean fromTheBeginning = from == null;
		// gets the relevant sensors list
		List<Sensor> sensorsOfInterest = metadata.getSensors(getAssociatedSensorKind());

		if (sensorsOfInterest.isEmpty()) {
			return;
		}

		List<ProjectIrrigationSpan> spansToUpdate = new ArrayList<ProjectIrrigationSpan>();
		// if not defined start timestamp, use the midnight of project activation date
		long actualStart;
		if (!fromTheBeginning) {
			actualStart = from;
		} else {
			actualStart = metadata.getTheFirstMeasurementTS();
		}

		if (to == null) {
			to = System.currentTimeMillis();
		}

		int length = (int) ((to - actualStart) / timeResolution);
		if (length <= 0) {
			return;
		}
		Map<Integer, int[]> allSensorsMeasurements = DBQueriesManagerSensors.getWaterPressureMeasurementsAsIntegerArray(
				sensorsOfInterest, PhytechApi.getProjectIgnoredDates(metadata, getAssociatedSensorKind()), actualStart,
				to, timeResolution);

		// lock given project in DB
		ProjectHourlyLock phl = new ProjectHourlyLock();
		phl.setProjectId(metadata.getProjectId());
		phl.setWorkerId(PropertiesManager.workerID);
		phl.setStart_date(actualStart);
		for (Integer sensorId : allSensorsMeasurements.keySet()) {
			int[] currentSensor = allSensorsMeasurements.get(sensorId);
			int lastIndex = 0;
			for (int h = currentSensor.length - 1; h >= 0; h--) {
				if (currentSensor[h] != 0) {
					lastIndex = h;
					break;
				}
			}
			phl.addSource(sensorId, actualStart + lastIndex * timeResolution);

		}
		phl.setCreated_at(System.currentTimeMillis());

		// try to read an exist lock
		ProjectHourlyLock phlExist = DBQueriesManager.getProjectHourlyLock(metadata.getProjectId(), actualStart);

		// compare an existing and new one locks
		if (phlExist != null) {
			int comp = phl.compareTo(phlExist);
			if (comp == -1) {
				// res.setReason(ResultReasonType.SHOULDNT_BE_CALCULATED);
				return;
			} else if (comp == 0) {
				// res.setReason(ResultReasonType.SEND_RECALC);
				return;

			}
		}

		// lock given project in DB
		DBQueriesManager.addProjectHourlyLock(phl);

		int deleted = DBQueriesManagerProjects.deleteIrrigationSpans(metadata.getProjectId(), actualStart);
		log.info("Deleted " + deleted + " spans");

		IrrigationData[] aggregatedIrrigation = Utils.calculateProjectIrrigation(metadata, sensorsOfInterest, length,
				allSensorsMeasurements);

		if (aggregatedIrrigation == null) {
			return;
		}
		List<SensorLocation> locations = DBQueriesManagerSensors.getSensorLocations(sensorsOfInterest.get(0).getID(),
				actualStart, to);

		if (locations.isEmpty()) {
			return;
		}

		spansToUpdate.addAll(splitToSpans(aggregatedIrrigation, locations, actualStart, metadata));

		if (PropertiesManager.isDebug()) {
			log.info("New irrigation spans:");
			for (ProjectIrrigationSpan is : spansToUpdate) {
				log.info(is.toString(metadata.getTimezone()));
			}
		}

		phlExist = DBQueriesManager.getProjectHourlyLock(metadata.getProjectId(), actualStart);
		Long now = System.currentTimeMillis();
		if (phlExist != null) {
			if (phlExist.getWorkerId() == PropertiesManager.workerID
					|| now - phlExist.getCreated_at() > Constants.TEN_MINUTES_MILLISECONDS) {
				DBQueriesManagerProjects.updateIrrigationSpans(metadata.getProjectId(), spansToUpdate);
				DBQueriesManager.deleteProjectHourlyLock(metadata.getProjectId(), actualStart);
				log.info("Updated " + spansToUpdate.size() + " spans");
			} else {
				log.debug("Denied to update");
			}
		}

	}

	private Collection<ProjectIrrigationSpan> splitToSpans(IrrigationData[] data, List<SensorLocation> locations,
			long startTs, ProjectMetadata metadata) {

		List<ProjectIrrigationSpan> spans = new ArrayList<ProjectIrrigationSpan>();
		// nothing to split
		if (data.length == 0) {
			return spans;
		}
		// update time
		long now = System.currentTimeMillis();
		SensorLocation[] locationsArray = convertLocationsToArray(locations, data.length, startTs, metadata);

		Direction direction = null;
		for (int i = 1; i < locationsArray.length; i++) {
			direction = MathUtils.getDirection(locationsArray[i - 1], locationsArray[i], direction);
			boolean isHoveringProject = MathUtils.isHoveringOn(locationsArray[i - 1].getAngle(),
					locationsArray[i].getAngle(), direction, metadata.getProjectAngle());
			if (isHoveringProject && data[i - 1].getIrrigation() == 1) {
				ProjectIrrigationSpan pSpan = new ProjectIrrigationSpan(metadata.getProjectId(),
						startTs + (long) (i - 1) * timeResolution, startTs + (long) (i) * timeResolution);
				pSpan.setClosed(true);
				pSpan.setUpdated_at(now);
				pSpan.setLast_measurement(locationsArray[i].getDate());

				if (metadata.getGpm() != null && metadata.getPivotRadius() != null) {
					int deltaTimeInMinutes = 60;// pSpan.getTimeSpan().getEndDate()-pSpan.getTimeSpan().getStartDate();
					double deltaAngle = Math.abs(locationsArray[i].getAngle() - locationsArray[i - 1].getAngle());
					double area = (Math.PI * Math.pow(metadata.getPivotRadius(), 2) / 360) * deltaAngle;
					pSpan.setAmount((float) (metadata.getGpm() * deltaTimeInMinutes / area));
				}

				spans.add(pSpan);
			}

		}

		return spans;
	}

	private SensorLocation[] convertLocationsToArray(List<SensorLocation> locations, int length, long startTs,
			ProjectMetadata metadata) {
		SensorLocation[] locationsArray = new SensorLocation[length];

		// convert list of locations to an array
		for (SensorLocation sl : locations) {
			int indexInArray = (int) ((sl.getDate() - startTs) / timeResolution);
			if (indexInArray < length) {
				locationsArray[indexInArray] = sl;
			}
		}

		// complete possible null's
		SensorLocation curent = locations.get(0);
		for (int i = 0; i < length; i++) {
			if (locationsArray[i] == null) {
				locationsArray[i] = curent;
			} else {
				curent = locationsArray[i];
			}
		}

		return locationsArray;
	}

	private Map<Long, Double> splitToDailyIrrigation(ProjectMetadata metadata, List<ProjectIrrigationSpan> spans) {

		Map<Long, Double> res = new HashMap<Long, Double>();
		for (ProjectIrrigationSpan span : spans) {
			Long start = span.getTimeSpan().getStartDate();
			Long end = span.getTimeSpan().getEndDate();

			List<Long> EOD = TimeUtils.getEODList(start, end, metadata.getTimezone());

			if (EOD.isEmpty()) {
				Long noon = TimeUtils.getNoonInTimeZone(start, metadata.getTimezone());
				Double dailyIrrigation = res.remove(noon);
				if (dailyIrrigation == null) {
					dailyIrrigation = 0.0;
				}

				if (span.getAmount() != null) {
					dailyIrrigation += span.getAmount();
				}

				res.put(noon, dailyIrrigation);
			} else {

				// the first day
				int irrigationLength = TimeUtils.getTenMinutesIntervalsBetween(end, start);
				double irrigationPer10Minutes = 0.0;

				if (span.getAmount() != null) {
					irrigationPer10Minutes = span.getAmount() / irrigationLength;
				}

				int lengthInTenMinutesIntervals = TimeUtils.getTenMinutesIntervalsBetween(start, EOD.get(0));
				Long noon = TimeUtils.getNoonInTimeZone(start, metadata.getTimezone());
				Double dailyIrrigation = res.remove(noon);
				if (dailyIrrigation == null) {
					dailyIrrigation = 0.0;
				}

				dailyIrrigation += lengthInTenMinutesIntervals * irrigationPer10Minutes;

				res.put(noon, dailyIrrigation);

				// from the second till day before last (if exists)
				for (int dayIndex = 1; dayIndex < EOD.size() - 2; dayIndex++) {

					noon = TimeUtils.getNoonInTimeZone(EOD.get(dayIndex) - 1, metadata.getTimezone());
					dailyIrrigation = res.remove(noon);
					if (dailyIrrigation == null) {
						dailyIrrigation = 0.0;
					}

					dailyIrrigation += 24 * 6 * irrigationPer10Minutes;

					res.put(noon, dailyIrrigation);

				}

				// the last day
				if (EOD.size() >= 2) {
					lengthInTenMinutesIntervals = TimeUtils.getTenMinutesIntervalsBetween(EOD.get(EOD.size() - 1), end);
					noon = TimeUtils.getNoonInTimeZone(EOD.get(EOD.size() - 1) - 1, metadata.getTimezone());
					dailyIrrigation = res.remove(noon);
					if (dailyIrrigation == null) {
						dailyIrrigation = 0.0;
					}
					dailyIrrigation += lengthInTenMinutesIntervals * irrigationPer10Minutes;

					res.put(noon, dailyIrrigation);

				}

			}

		}

		return res;
	}

	@Override
	protected List<Measurement> calcGraphValue(Measurement[] measurements, Measurement prevAverage) {
		return new ArrayList<Measurement>();
	}

	public void recalculateDailyIrrigation(Integer projectId, Long start, Long end) {
		ProjectMetadata metadata = PhytechApi.getProjectMetaData(projectId);
		List<ProjectIrrigationSpan> spans = DBQueriesManagerProjects.getIrrigationSpans(projectId, start, end);
		DBQueriesManagerProjects.upsertDailyIrigation(metadata.getProjectId(), splitToDailyIrrigation(metadata, spans));
	}

}
