package com.phytech.PhytechHourlyWorker;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.phytech.database.DBQueriesManagerProjects;
import com.phytech.database.DBQueriesManagerSensors;
import com.phytech.model.Measurement;
import com.phytech.model.ProjectMetadata;
import com.phytech.model.Enumerations.SensorKind;
import com.phytech.model.Sensors.Sensor;
import com.phytech.utils.PhytechApi;
import com.phytech.utils.PropertiesManager;

public abstract class HourlyCalculator {
	private static final Logger log = LogManager.getLogger(HourlyCalculator.class);
	protected Measurement[] prevValid = null;

	protected abstract List<Measurement> calcGraphValue(Measurement[] measurements, Measurement prevGraphValue);

	public Measurement getAnchorForNewGraph(long ts) {
		return null;
	}

	protected boolean isValidMeasurement(Measurement m) {
		return true;
	}

	protected abstract List<SensorKind> getAssociatedSensorKind();

	/**
	 * When continue the existing graph we need to have a set of prev. valid sensors
	 * per each sensor In the common case this will be the first element in
	 * measurements arrays list
	 * 
	 * @param metadata
	 *            - Project metadata
	 * @param measurementsByTS
	 * @param from
	 * @param end
	 */
	protected void initPrevValid(ProjectMetadata metadata, List<Measurement[]> measurementsByTS, Long from, Long end) {
		if (!measurementsByTS.isEmpty()) {
			prevValid = measurementsByTS.get(0);
		}
	}

	/**
	 * Calculates the average of measurements among with null test
	 * 
	 * @param measurements
	 * @return
	 */
	protected Measurement getAverage(Measurement[] measurements) {
		Double sum = 0.0;
		int validSensorsCounter = 0;
		long ts = 0;
		for (int sensorIndex = 0; sensorIndex < measurements.length; sensorIndex++) {
			if (isValidMeasurement(measurements[sensorIndex])) {
				sum += measurements[sensorIndex].getStitched();
				validSensorsCounter++;
				ts = measurements[sensorIndex].getTimestamp();

			}
		}

		if (validSensorsCounter > 0 && !Double.isNaN(sum)) {
			Measurement average = new Measurement(ts, 0.0);
			average.setStitched(sum / validSensorsCounter);
			average.setFormat(sum / validSensorsCounter);
			return average;
		}

		return null;

	}

	public void calc(ProjectMetadata metadata, Long from, Long to) {
		// gets the relevant sensors list
		List<Sensor> sensorsOfInterest = metadata.getSensors(getAssociatedSensorKind());

		if (sensorsOfInterest.isEmpty()) {
			return;
		}

		// if not defined start timestamp, use the midnight of project activation date
		long actualStart;
		if (from != null) {
			actualStart = from;
		} else {
			actualStart = DBQueriesManagerSensors.getFirstMeasurementAfter(sensorsOfInterest,
					metadata.getTheFirstMeasurementTS());
		}

		prevValid = new Measurement[sensorsOfInterest.size()];


		List<Measurement> averageData = new ArrayList<Measurement>();
		Measurement prevAverage = DBQueriesManagerProjects.getLastProjectHourlyData(metadata, actualStart,
				getAssociatedSensorKind().get(0));
		long firtsTSToGetMeasurements;
		int startIndex = 0;

		boolean continueExistingGraph = (prevAverage != null && from != null);
		if (continueExistingGraph) {
			firtsTSToGetMeasurements = prevAverage.getTimestamp();
			startIndex = 1;
		} else {
			firtsTSToGetMeasurements = actualStart;
		}

		// gets the data of all sensors aggregated by timestamp (in the very common case
		// - array of 3 measurements)
		List<Measurement[]> measurementsByTS = DBQueriesManagerSensors.getMeasurementsByTimestamp(
				PhytechApi.getProjectIgnoredDates(metadata, getAssociatedSensorKind()), firtsTSToGetMeasurements, to);

		if (continueExistingGraph && !measurementsByTS.isEmpty()) {
			log.info("Continue " + getAssociatedSensorKind() + " graph after "
					+ prevAverage.toString(metadata.getTimezone()));
			// add the previous graph value to list of averages
			averageData.add(prevAverage);

			// for delta calculations
			initPrevValid(metadata, measurementsByTS, metadata.getTheFirstMeasurementTS(), firtsTSToGetMeasurements);

			if (PropertiesManager.isDebug()) {
				log.info("Previous valid sensor:");
				for (int i = 0; i < prevValid.length; i++) {
					if (prevValid[i] == null) {
						log.info("Sensor " + sensorsOfInterest.get(i).getID() + " null");
					} else {
						log.info("Sensor " + sensorsOfInterest.get(i).getID()
								+ prevValid[i].toString(metadata.getTimezone()));
					}
				}
			}

		} else {
			log.info("Start new " + getAssociatedSensorKind());
			Measurement anchor = getAnchorForNewGraph(firtsTSToGetMeasurements);
			if (anchor != null) {
				log.info("The anchor is " + anchor.toString(metadata.getTimezone()));

				averageData.add(anchor);
				prevAverage = anchor;
			} else {
				// Anchor this is a the first possible real average of data - look for it
				for (int tsIndex = 0; tsIndex < measurementsByTS.size(); tsIndex++) {
					anchor = getAverage(measurementsByTS.get(tsIndex));
					startIndex = tsIndex;
					if (anchor != null) {
						for (int i = 0; i < sensorsOfInterest.size(); i++) {
							if (isValidMeasurement(measurementsByTS.get(tsIndex)[i])) {
								prevValid[i] = measurementsByTS.get(tsIndex)[i];
							}
						}
						break;
					}
				}

				// if found some valid
				if (anchor != null) {
					// log.info("The anchor is " + anchor.toString(metadata.getTimezone()));
					averageData.add(anchor);
				}
			}
			prevAverage = anchor;

		}

		// run through time line
		for (int hourIndex = startIndex; hourIndex < measurementsByTS.size(); hourIndex++) {

			List<Measurement> values = calcGraphValue(measurementsByTS.get(hourIndex), prevAverage);
			for (Measurement m : values) {
				if (m != null) {
					prevAverage = m;
					averageData.add(m);
				}
			}
		}

		boolean isWriteAllowed = true;
		// DBQueriesManager.checkWorkerLock(metadata.getProjectId(),
		// CalculationKind.PROJECT_HOURLY, PropertiesManager.workerID, startTS);

		if (isWriteAllowed) {
			// write calculations to the DB and remove the lock
			DBQueriesManagerProjects.saveHourlyData(metadata.getProjectId(), averageData,
					getAssociatedSensorKind().get(0));
			// DBQueriesManager.deleteWorkerLock(metadata.getProjectId(),
			// CalculationKind.PROJECT_HOURLY,
			// PropertiesManager.workerID, startTS);

		} else {
			log.warn("Can't update hourly calculations for project " + metadata.getProjectId()
					+ " since it's already locked");
		}

	}

}