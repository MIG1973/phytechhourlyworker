package com.phytech.PhytechHourlyWorker;

import java.util.ArrayList;
import java.util.List;

import com.phytech.model.Measurement;
import com.phytech.model.Enumerations.SensorKind;
import com.phytech.utils.Constants;

public class PlantCalculator extends HourlyCalculator {

	@Override
	protected List<SensorKind> getAssociatedSensorKind() {
		List<SensorKind> kinds = new ArrayList<>();
		kinds.add(SensorKind.PLANT);
		return kinds;
	}

	@Override
	protected List<Measurement> calcGraphValue(Measurement[] measurements, Measurement prevAverage) {
		double maxDelta = 100.0;
		List<Measurement> res = new ArrayList<Measurement>();

		// average is calculated as the previous data + average of delta per sensor
		// this is done to relax spikes in data, caused by different number of sensors
		// taken to average calculation from point to point

		Double deltaSum = 0.0;
		int validSensorsCounter = 0;
		long ts = 0;
		for (int sensorIndex = 0; sensorIndex < measurements.length; sensorIndex++) {
			if (measurements[sensorIndex] != null && prevValid[sensorIndex] != null) {
				double currentSensorDelta = (measurements[sensorIndex].getStitched()
						- prevValid[sensorIndex].getStitched());

				// only small deltas are valid
				if (Math.abs(currentSensorDelta) <= maxDelta) {
					deltaSum += currentSensorDelta;
					validSensorsCounter++;
					ts = measurements[sensorIndex].getTimestamp();
				}
			}
		}

		// "interpolate" gaps
		long deltaTime = ts - prevAverage.getTimestamp();
		if (deltaTime > Constants.ONE_HOUR_MILLISECONDS) {
			for (long dummy = prevAverage.getTimestamp()
					+ Constants.ONE_HOUR_MILLISECONDS; dummy < ts; dummy += Constants.ONE_HOUR_MILLISECONDS) {
				Measurement average = new Measurement(dummy, 0.0);

				average.setFormat(prevAverage.getFormat());
				average.setStitched(prevAverage.getFormat());
				res.add(average);
			}
		}

		if (validSensorsCounter > 0) {
			Measurement average = new Measurement(ts, 0.0);
			double averageDelta = deltaSum / validSensorsCounter;

			// if an average delta is too big
			if (Math.abs(averageDelta) > maxDelta) {
				averageDelta = 0.0;
			}

			average.setFormat(prevAverage.getFormat() + averageDelta);
			average.setStitched(prevAverage.getFormat() + averageDelta);
			res.add(average);
			prevValid = measurements;
		} else {
			// handle the case with no valid delta at all
			Measurement average = new Measurement(prevAverage.getTimestamp() + Constants.ONE_HOUR_MILLISECONDS, 0.0);

			average.setFormat(prevAverage.getFormat());
			average.setStitched(prevAverage.getFormat());
			res.add(average);
			prevValid = measurements;

		}

		return res;

	}

	@Override
	public Measurement getAnchorForNewGraph(long ts) {
		Measurement anchor = new Measurement(ts, 1000.0);
		anchor.setFormat(1000.0);
		anchor.setStitched(1000.0);

		return anchor;
	}

}
