package com.phytech.PhytechHourlyWorker;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.phytech.database.DBQueriesManager;
import com.phytech.database.DBQueriesManagerProjects;
import com.phytech.database.DBQueriesManagerSensors;
import com.phytech.model.IrrigationData;
import com.phytech.model.Measurement;
import com.phytech.model.ProjectHourlyLock;
import com.phytech.model.ProjectIrrigationSpan;
import com.phytech.model.ProjectMetadata;
import com.phytech.model.Enumerations.SensorKind;
import com.phytech.model.Sensors.Sensor;
import com.phytech.utils.Constants;
import com.phytech.utils.MathUtils;
import com.phytech.utils.PhytechApi;
import com.phytech.utils.PropertiesManager;
import com.phytech.utils.TimeUtils;
import com.phytech.utils.Utils;

public class WaterPresureHourlyCalculator extends HourlyCalculator {
	private static final Logger log = LogManager.getLogger(WaterPresureHourlyCalculator.class);

	@Override
	protected List<SensorKind> getAssociatedSensorKind() {
		List<SensorKind> kinds = new ArrayList<>();
		kinds.add(SensorKind.WATER_PRESSURE);
		kinds.add(SensorKind.IRRIGATION);
		return kinds;
	}

	@Override
	public void calc(ProjectMetadata metadata, Long from, Long to) {

		boolean fromTheBeginning = from == null;
		// gets the relevant sensors list
		List<Sensor> sensorsOfInterest = metadata.getSensors(getAssociatedSensorKind());

		if (sensorsOfInterest.isEmpty()) {
			return;
		}

		List<ProjectIrrigationSpan> spansToUpdate = new ArrayList<ProjectIrrigationSpan>();
		// if not defined start timestamp, use the midnight of project activation date
		long actualStart;
		if (!fromTheBeginning) {
			actualStart = from;
		} else {
			actualStart = metadata.getTheFirstMeasurementTS();
		}

		if (to == null) {
			to = System.currentTimeMillis();
		}

		ProjectIrrigationSpan overlapSpan = DBQueriesManagerProjects.getOverlapedSpan(metadata.getProjectId(),
				actualStart);

		if (overlapSpan != null) {
			log.debug("Affected from " + overlapSpan.toString(metadata.getTimezone()));
		} else {
			log.debug("No affected spans");
		}

		if (!fromTheBeginning && overlapSpan != null) {
			if (!fromTheBeginning && overlapSpan != null) {
				actualStart = overlapSpan.getTimeSpan().getStartDate();
			}
		}

		int length = (int) ((to - actualStart) / Constants.TEN_MINUTES_MILLISECONDS);

		if (length <= 0) {
			return;
		}
		Map<Integer, int[]> allSensorsMeasurements = DBQueriesManagerSensors.getWaterPressureMeasurementsAsIntegerArray(
				sensorsOfInterest, PhytechApi.getProjectIgnoredDates(metadata, getAssociatedSensorKind()), actualStart,
				to);

		// lock given project in DB
		ProjectHourlyLock phl = new ProjectHourlyLock();
		phl.setProjectId(metadata.getProjectId());
		phl.setWorkerId(PropertiesManager.workerID);
		phl.setStart_date(actualStart);
		for (Integer sensorId : allSensorsMeasurements.keySet()) {
			int[] currentSensor = allSensorsMeasurements.get(sensorId);
			int lastIndex = 0;
			for (int h = currentSensor.length - 1; h >= 0; h--) {
				if (currentSensor[h] != 0) {
					lastIndex = h;
					break;
				}
			}
			phl.addSource(sensorId, actualStart + lastIndex * Constants.TEN_MINUTES_MILLISECONDS);

		}
		phl.setCreated_at(System.currentTimeMillis());

		// try to read an exist lock
		ProjectHourlyLock phlExist = DBQueriesManager.getProjectHourlyLock(metadata.getProjectId(), actualStart);

		// compare an existing and new one locks
		if (phlExist != null) {
			int comp = phl.compareTo(phlExist);
			if (comp == -1) {
				// res.setReason(ResultReasonType.SHOULDNT_BE_CALCULATED);
				return;
			} else if (comp == 0) {
				// res.setReason(ResultReasonType.SEND_RECALC);
				return;

			}
		}

		// lock given project in DB
		DBQueriesManager.addProjectHourlyLock(phl);

		int deleted = DBQueriesManagerProjects.deleteIrrigationSpans(metadata.getProjectId(), actualStart);
		log.info("Deleted " + deleted + " spans");

		IrrigationData[] aggregatedIrrigation = Utils.calculateProjectIrrigation(metadata, sensorsOfInterest, length,
				allSensorsMeasurements);

		if (aggregatedIrrigation == null) {
			return;
		}

		spansToUpdate.addAll(splitToSpans(aggregatedIrrigation, actualStart, metadata));

		if (PropertiesManager.isDebug()) {
			log.info("New irrigation spans:");
			for (ProjectIrrigationSpan is : spansToUpdate) {
				log.info(is.toString(metadata.getTimezone()));
			}
		}

		phlExist = DBQueriesManager.getProjectHourlyLock(metadata.getProjectId(), actualStart);
		Long now = System.currentTimeMillis();
		if (phlExist != null) {
			if (phlExist.getWorkerId() == PropertiesManager.workerID
					|| now - phlExist.getCreated_at() > Constants.TEN_MINUTES_MILLISECONDS) {
				DBQueriesManagerProjects.updateIrrigationSpans(metadata.getProjectId(), spansToUpdate);
				DBQueriesManager.deleteProjectHourlyLock(metadata.getProjectId(), actualStart);
				log.info("Updated " + spansToUpdate.size() + " spans");
			} else {
				log.debug("Denied to update");
			}
		}

	}

	private Collection<ProjectIrrigationSpan> splitToSpans(IrrigationData[] data, long startTs,
			ProjectMetadata metadata) {

		if (metadata.getSensors(SensorKind.IRRIGATION).size() > 0) {
			return splitToSpansWithPulses(data, startTs, metadata);
		} else {
			return splitToSpansWithoutPulses(data, startTs, metadata);
		}
	}

	/**
	 * Split Irrigation data buffer when at least one sensor is IRRIGATIOn (i.e
	 * shall send start/end pulses) In this cases only positive info open/close
	 * spans (ignore NO DATA)
	 * 
	 * @param data
	 * @param startTs
	 * @param metadata
	 * @return
	 */
	private Collection<ProjectIrrigationSpan> splitToSpansWithPulses(IrrigationData[] data, long startTs,
			ProjectMetadata metadata) {

		List<ProjectIrrigationSpan> spans = new ArrayList<ProjectIrrigationSpan>();

		// nothing to split
		if (data.length == 0) {
			return spans;
		}

		if (data.length > 2) {
			for (int i = 2; i < data.length; i++) {
				if (data[i - 2].getIrrigation() == -1 && data[i - 1].getIrrigation() == 1
						&& data[i].getIrrigation() == -1) {
					data[i - 1].setIrrigation(-1);
					log.debug("False start pulse detected at " + TimeUtils.getDateTimeString(
							startTs + (long) ((i - 1) * Constants.TEN_MINUTES_MILLISECONDS), metadata.getTimezone()));
				}

				if (data[i - 2].getIrrigation() == 1 && data[i - 1].getIrrigation() == -1
						&& data[i].getIrrigation() == 1) {
					data[i - 1].setIrrigation(1);
					log.debug("False end pulse detected at " + TimeUtils.getDateTimeString(
							startTs + (long) ((i - 1) * Constants.TEN_MINUTES_MILLISECONDS), metadata.getTimezone()));
				}

			}
		}

		// update time
		long now = System.currentTimeMillis();

		int spanStart = 0; // the index of span start
		boolean isCurrentOpen = false;
		if (data[0].getIrrigation() == 1) {
			isCurrentOpen = true;
		}

		Float currentAmount = null;

		for (int i = 1; i < data.length; i++) {

			// signal is changed
			if (data[i].getIrrigation() != data[spanStart].getIrrigation()) {

				if (data[i].getIrrigation() == 1) {
					// the signal switched from to true, means we're starting new span
					spanStart = i;
					isCurrentOpen = true;
					currentAmount = (float) 0;

				} else if (data[i].getIrrigation() == -1 && isCurrentOpen) {
					// the signal switched from to false , means we're closing a
					// current span
					ProjectIrrigationSpan span = new ProjectIrrigationSpan(metadata.getProjectId(),
							startTs + (long) (spanStart) * Constants.TEN_MINUTES_MILLISECONDS,
							startTs + (long) (i) * Constants.TEN_MINUTES_MILLISECONDS);

					if (data[i].getIrrigation() == -1) {
						span.setClosed(true);
					}

					span.setAmount(currentAmount);
					span.setUpdated_at(now);
					span.setLast_measurement(span.getTimeSpan().getEndDate());
					spans.add(span);
					spanStart = i;
					isCurrentOpen = false;

				}

			}

			if (isCurrentOpen) {
				currentAmount = MathUtils.floatNullSafeSum(currentAmount, data[i].getAmmount());
			}
		}

		if (isCurrentOpen) {
			ProjectIrrigationSpan span = new ProjectIrrigationSpan(metadata.getProjectId(),
					startTs + (long) spanStart * (long)Constants.TEN_MINUTES_MILLISECONDS, null);

			span.setAmount(currentAmount);
			span.setUpdated_at(now);
			span.setLast_measurement(startTs + (long) (data.length - 1) * Constants.TEN_MINUTES_MILLISECONDS);
			spans.add(span);

		}

		// run for all created spans except the last one and force close it
		for (int i = 0; i < spans.size() - 1; i++) {
			spans.get(i).setClosed(true);
		}

		if (!spans.isEmpty() && !spans.get(spans.size() - 1).isClosed()) {
			spans.get(spans.size() - 1).setEnd(null);
		}

		return spans;
	}

	/**
	 * Split Irrigation data buffer when all sensors is WATER PRESSURE (i.e no
	 * pulses) In this cases NO DATA is treated as no irrigation
	 * 
	 * @param data
	 * @param startTs
	 * @param metadata
	 * @return
	 */
	private Collection<ProjectIrrigationSpan> splitToSpansWithoutPulses(IrrigationData[] data, long startTs,
			ProjectMetadata metadata) {
		List<ProjectIrrigationSpan> spans = new ArrayList<ProjectIrrigationSpan>();

		// nothing to split
		if (data.length == 0) {
			return spans;
		}

		// update time
		long now = System.currentTimeMillis();

		int spanStart = 0; // the index of span start
		boolean isCurrentOpen = false;
		if (data[0].getIrrigation() == 1) {
			isCurrentOpen = true;
		}

		Float currentAmount = null;

		for (int i = 1; i < data.length; i++) {

			// signal is changed
			if (data[i].getIrrigation() != data[spanStart].getIrrigation()) {

				if (data[i].getIrrigation() == 1) {
					// the signal switched from to true, means we're starting new span
					spanStart = i;
					isCurrentOpen = true;
					currentAmount = data[i].getAmmount();
				} else if ((data[i].getIrrigation() + data[spanStart].getIrrigation() == -1)) {
					// switch from NO IRR to NO DATA or NO DATA to NO IRR ==> close last known span
					if (isCurrentOpen) {
						isCurrentOpen = false;
					}
					if (!spans.isEmpty()) {
						spans.get(spans.size() - 1).setClosed(true);
					}
					continue;
				} else {

					// the signal switched from true to false or NULL , means we're closing a
					// current span
					ProjectIrrigationSpan span = new ProjectIrrigationSpan(metadata.getProjectId(),
							startTs + (long) (spanStart) * Constants.TEN_MINUTES_MILLISECONDS,
							startTs + (long) (i) * Constants.TEN_MINUTES_MILLISECONDS);

					if (data[i].getIrrigation() == -1) {
						span.setClosed(true);
					}

					span.setAmount(currentAmount);
					span.setUpdated_at(now);
					span.setLast_measurement(span.getTimeSpan().getEndDate());
					spans.add(span);
					spanStart = i;
					isCurrentOpen = false;
				}
			} else {
				if (isCurrentOpen) {
					currentAmount = MathUtils.floatNullSafeSum(currentAmount, data[i].getAmmount());
				}
			}
		}

		if (isCurrentOpen) {
			ProjectIrrigationSpan span = new ProjectIrrigationSpan(metadata.getProjectId(),
					startTs + (long) (spanStart * Constants.TEN_MINUTES_MILLISECONDS), null);

			span.setAmount(currentAmount);
			span.setUpdated_at(now);
			span.setLast_measurement(startTs + (long) (data.length - 1) * Constants.TEN_MINUTES_MILLISECONDS);
			spans.add(span);

		}

		// run for all created spans except the last one and force close it
		for (int i = 0; i < spans.size() - 1; i++) {
			spans.get(i).setClosed(true);
		}

		if (!spans.isEmpty() && !spans.get(spans.size() - 1).isClosed()) {
			spans.get(spans.size() - 1).setEnd(null);
		}

		return spans;
	}

	private Map<Long, Double> splitToDailyIrrigation(ProjectMetadata metadata, List<ProjectIrrigationSpan> spans) {

		Map<Long, Double> res = new HashMap<Long, Double>();
		for (ProjectIrrigationSpan span : spans) {
			Long start = span.getTimeSpan().getStartDate();
			Long end = span.getTimeSpan().getEndDate();

			List<Long> EOD = TimeUtils.getEODList(start, end, metadata.getTimezone());

			if (EOD.isEmpty()) {
				Long noon = TimeUtils.getNoonInTimeZone(start, metadata.getTimezone());
				Double dailyIrrigation = res.remove(noon);
				if (dailyIrrigation == null) {
					dailyIrrigation = 0.0;
				}

				if (span.getAmount() != null) {
					dailyIrrigation += span.getAmount();
				}

				res.put(noon, dailyIrrigation);
			} else {

				// the first day
				int irrigationLength = TimeUtils.getTenMinutesIntervalsBetween(end, start);
				double irrigationPer10Minutes = 0.0;

				if (span.getAmount() != null) {
					irrigationPer10Minutes = span.getAmount() / irrigationLength;
				}

				int lengthInTenMinutesIntervals = TimeUtils.getTenMinutesIntervalsBetween(start, EOD.get(0));
				Long noon = TimeUtils.getNoonInTimeZone(start, metadata.getTimezone());
				Double dailyIrrigation = res.remove(noon);
				if (dailyIrrigation == null) {
					dailyIrrigation = 0.0;
				}

				dailyIrrigation += lengthInTenMinutesIntervals * irrigationPer10Minutes;

				res.put(noon, dailyIrrigation);

				// from the second till day before last (if exists)
				for (int dayIndex = 1; dayIndex < EOD.size() - 2; dayIndex++) {

					noon = TimeUtils.getNoonInTimeZone(EOD.get(dayIndex) - 1, metadata.getTimezone());
					dailyIrrigation = res.remove(noon);
					if (dailyIrrigation == null) {
						dailyIrrigation = 0.0;
					}

					dailyIrrigation += 24 * 6 * irrigationPer10Minutes;

					res.put(noon, dailyIrrigation);

				}

				// the last day
				if (EOD.size() >= 2) {
					lengthInTenMinutesIntervals = TimeUtils.getTenMinutesIntervalsBetween(EOD.get(EOD.size() - 1), end);
					noon = TimeUtils.getNoonInTimeZone(EOD.get(EOD.size() - 1) - 1, metadata.getTimezone());
					dailyIrrigation = res.remove(noon);
					if (dailyIrrigation == null) {
						dailyIrrigation = 0.0;
					}
					dailyIrrigation += lengthInTenMinutesIntervals * irrigationPer10Minutes;

					res.put(noon, dailyIrrigation);

				}

			}

		}

		return res;
	}

	@Override
	protected List<Measurement> calcGraphValue(Measurement[] measurements, Measurement prevAverage) {
		return new ArrayList<Measurement>();
	}

	public void recalculateDailyIrrigation(Integer projectId, Long start, Long end) {
		ProjectMetadata metadata = PhytechApi.getProjectMetaData(projectId);
		List<ProjectIrrigationSpan> spans = DBQueriesManagerProjects.getIrrigationSpans(projectId, start, end);
		DBQueriesManagerProjects.upsertDailyIrigation(metadata.getProjectId(), splitToDailyIrrigation(metadata, spans));
	}

}
