package hourlyWorker_runner;

import com.phytech.PhytechHourlyWorker.HourlyCalculator;
import com.phytech.PhytechHourlyWorker.PivotProjectHourlyCalculator;
import com.phytech.PhytechHourlyWorker.FruitCalculator;
import com.phytech.PhytechHourlyWorker.PlantCalculator;
import com.phytech.PhytechHourlyWorker.SoilMoistureHourlyCalculator;
import com.phytech.PhytechHourlyWorker.WaterPresureHourlyCalculator;
import com.phytech.model.Enumerations.SensorKind;

public class CalculatorsFactory {
	public static HourlyCalculator getCalculatorBySensorKind(SensorKind kind) {
		HourlyCalculator calculator = null;
		switch (kind) {
		case FRUIT:
			calculator = new FruitCalculator();
			break;
		case PLANT:
			calculator = new PlantCalculator();
			break;
		case WATER_PRESSURE:
		case IRRIGATION:
			calculator = new WaterPresureHourlyCalculator();
			break;
		case SOIL_MOISTURE:
			calculator = new SoilMoistureHourlyCalculator();
			break;
		case PIVOT:
			calculator = new PivotProjectHourlyCalculator();
			break;
		case SOIL_TEMPERATURE:
		case UNKNOWN:
		default:
			break;

		}

		return calculator;
	}
}
