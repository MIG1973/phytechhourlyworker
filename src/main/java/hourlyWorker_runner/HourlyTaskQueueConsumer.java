package hourlyWorker_runner;

import java.io.IOException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.phytech.PhytechHourlyWorker.HourlyCalculator;
import com.phytech.PhytechHourlyWorker.PivotSensorCalculator;
import com.phytech.PhytechHourlyWorker.PlantSensorCalculator;
import com.phytech.PhytechHourlyWorker.FruitCalculator;
import com.phytech.PhytechHourlyWorker.PlantCalculator;
import com.phytech.PhytechHourlyWorker.SoilMoistureHourlyCalculator;
import com.phytech.PhytechHourlyWorker.WaterPresureHourlyCalculator;
import com.phytech.database.DBQueriesManagerProjects;
import com.phytech.model.ProjectMetadata;
import com.phytech.model.WorkerTask;
import com.phytech.model.Enumerations.EventType;
import com.phytech.model.Enumerations.FailureReason;
import com.phytech.model.Enumerations.SensorKind;
import com.phytech.model.Sensors.Sensor;
import com.phytech.utils.Constants;
import com.phytech.utils.PhytechApi;
import com.phytech.utils.PropertiesManager;
import com.phytech.utils.QueueSender;
import com.phytech.utils.Utils;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class HourlyTaskQueueConsumer extends DefaultConsumer {

	private static final Logger log = LogManager.getLogger(HourlyTaskQueueConsumer.class);
	// private QueueSender tasksQueueSender = null;

	public HourlyTaskQueueConsumer(Channel channel, QueueSender sender) {
		super(channel);
		// this.tasksQueueSender = sender;

		if (PropertiesManager.isDebug()) {
//			ProjectMetadata ppp =  PhytechApi.getProjectMetaData(35131);
//			PivotSensorCalculator calculator = new PivotSensorCalculator();
//			calculator.updateBlueLine(ppp, 538511);
			
			// PhytechApi.initAllProjectsMetadata();
			@SuppressWarnings("unused")
			String testMsg;
			//ProjectMetadata metadata = PhytechApi.getProjectMetaData(46857);
			//WorkerTask wt = new WorkerTask(WorkerTaskType.FULL_RECALCULATE_PROJECT_HOURLY);
			//wt.setProjectId(54271);
			//wt.setSensorId(605223);
			//wt.setDate(TimeUtils.getMidnightTimeZone(15, 3, 2019, metadata.getTimezone()));
			//wt.setDate(null);
			// handleProjectTask(wt);

//			PhytechApi.initAllProjectsMetadata();
//			testMsg = "{\"date\":1528498800000,\"taskType\":\"CALC_PROJECT_HOURLY\",\"refresh_metadata\":false,\"priority\":2,\"projectId\":35845,\"taskId\":\"\",\"sensorId\":538511}";
//			handleMessage(testMsg);
//			testMsg = "{\"date\":1528498800000,\"taskType\":\"CALC_PROJECT_HOURLY\",\"refresh_metadata\":false,\"priority\":2,\"projectId\":35846,\"taskId\":\"\",\"sensorId\":538511}";
//			handleMessage(testMsg);
//			testMsg = "{\"date\":1528498800000,\"taskType\":\"CALC_PROJECT_HOURLY\",\"refresh_metadata\":false,\"priority\":2,\"projectId\":35847,\"taskId\":\"\",\"sensorId\":538511}";
//			handleMessage(testMsg);
//			
//			testMsg = "{\"date\":1527829200000,\"taskType\":\"CALC_PROJECT_HOURLY\",\"refresh_metadata\":false,\"priority\":2,\"projectId\":35131,\"taskId\":\"\",\"sensorId\":543014}";
//			handleMessage(testMsg);
//
//			testMsg = "{\"date\":1527829200000,\"taskType\":\"CALC_PROJECT_HOURLY\",\"refresh_metadata\":false,\"priority\":2,\"projectId\":36237,\"taskId\":\"\",\"sensorId\":543014}";
//			handleMessage(testMsg);
			
			PhytechApi.getProjectMetaData(58090,false);
			testMsg ="{\"date\":1557842400000,\"taskType\":\"CALC_PROJECT_HOURLY\",\"priority\":2,\"projectId\":58090,\"taskId\":\"\",\"sensorId\":658812}";
			handleMessage(testMsg);
			testMsg ="{\"date\":1557842400000,\"taskType\":\"CALC_PIVOT_LINES\",\"priority\":2,\"projectId\":58090,\"taskId\":\"\",\"sensorId\":658812}";
			handleMessage(testMsg);
			

		}

		log.info("Start Hourly Worker...");
	}

	@Override
	public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
			throws IOException {
		String message = new String(body, "UTF-8");
		log.debug(" [x] Received '" + message + "'");

		long deliveryTag = envelope.getDeliveryTag();
		Channel channel = getChannel();

		handleMessage(message);
		try {
			channel.basicAck(deliveryTag, false);
			log.trace("ACK sent successfully for the task with a delivery tag " + deliveryTag);
		} catch (java.net.SocketException se) {
			log.error("ACK problem " + se.getMessage());
		} catch (com.rabbitmq.client.AlreadyClosedException ace) {
			log.error("ACK problem " + ace.getMessage());
		}

	}

	protected boolean handleMessage(String message) {
		WorkerTask task = new WorkerTask(message);
		switch (task.getTaskType()) {
		// might be send from endpoint only
		case RECALCULATE_SENSOR_HOURLY_DATA:
		case CALC_PIVOT_LINES:
			return handleSensorTask(task);

		case RECALCULATE_DAILY_IRRIGATION:
			handeleRecalculateDailyIrrigation(task.getProjectId(), task.getDate(), null);
			return true;

		case CALC_PROJECT_HOURLY:
		case RECALCULATE_PROJECT_HOURLY:
		case FULL_RECALCULATE_PROJECT_HOURLY:
			return handleProjectTask(task);

		default:
			return true;
		}

	}

	private boolean handleSensorTask(WorkerTask task) {
		try {
			// prepare metadata objects
			ProjectMetadata metadata = null;

			if (task.getProjectId() != null && task.getProjectId() != 0) {
				metadata = PhytechApi.getProjectMetaData(task.getProjectId(),false);

			} else if (task.getSensorId() != null) {
				return true;
			}
			Sensor sensor = metadata.getSensor(task.getSensorId());

			if (sensor == null) {
				Utils.logSensorEvent(task.getSensorId(), EventType.SENSOR_CALC_FAILED, task.getDate(),
						FailureReason.CANT_FOUND_METADATA, null);
				return true;
			}

			
			switch (task.getTaskType()) {
			case RECALCULATE_SENSOR_HOURLY_DATA:
				if (task.getDate()>System.currentTimeMillis()) {
					return true;
				}
				// recalculate from task date till now
				if (sensor.getSensorKind().equals(SensorKind.PLANT)) {
					new PlantSensorCalculator().recalculateSensorHourlyData(sensor.getID(), metadata, task.getDate(),
							null);
				}
				break;
				
			case CALC_PIVOT_LINES:
				PivotSensorCalculator bluLineCalculator = new PivotSensorCalculator();
				bluLineCalculator.updateBlueLine(metadata, task.getSensorId());
				break;


			default:
				return false;
			}

		} catch (

		Exception e) {
			log.error("Error handling message", e);
			return false;
		}

		return true;

	}

	private boolean handleProjectTask(WorkerTask task) {
		try {
			// prepare metadata objects
			ProjectMetadata metadata = PhytechApi.getProjectMetaData(task.getProjectId());


			if (metadata == null) {
				Utils.logProjectEvent(task.getProjectId(), EventType.PROJECT_CALC_FAILED, task.getDate(),
						FailureReason.CANT_FOUND_METADATA, null);
				return false;
			}

			// Shall not calculate before activation date
			if (task.getDate() != null && task.getDate() < metadata.getTheFirstMeasurementTS()) {
				return true;
			}
			boolean recalcPlant = false;
			boolean recalcWater = false;
			boolean recalcFruit = false;

			Sensor trigger = metadata.getSensor(task.getSensorId());

			switch (task.getTaskType()) {
			case RECALCULATE_DAILY_IRRIGATION:
				// utility task to calculate back the project daily irrigation
				handeleRecalculateDailyIrrigation(task.getProjectId(), task.getDate(), null);

			case CALC_PROJECT_HOURLY:
				// ongoing hourly calculations for the specified project;
				if (trigger == null) {
					// this shouldn't happen
					log.warn("Can't perform ongoing calc - trigger is null");
					return true;
				}

				// get the appropriate calculator
				HourlyCalculator calculator = CalculatorsFactory.getCalculatorBySensorKind(trigger.getSensorKind());

				// no calculator found
				if (calculator == null) {
					return true;
				}
				long startCalcDate = task.getDate();
				switch (trigger.getSensorKind()) {
				case FRUIT:
					// for fruit calculations move start date 3 days before
					startCalcDate = Math.max(metadata.getTheFirstMeasurementTS(),
							task.getDate() - Constants.ONE_DAY_MILLISECONDS * 3);
					break;
				case PLANT:
					// for plant calculations move start date 1 days before
					startCalcDate = Math.max(metadata.getTheFirstMeasurementTS(),
							task.getDate() - Constants.ONE_DAY_MILLISECONDS);
					break;
				case WATER_PRESSURE:
				case IRRIGATION:
					startCalcDate = Math.max(metadata.getTheFirstMeasurementTS(),
							task.getDate() - Constants.ONE_DAY_MILLISECONDS * 3);
					break;
					
				case PIVOT:
					startCalcDate = Math.max(metadata.getTheFirstMeasurementTS(),
							task.getDate() - Constants.ONE_DAY_MILLISECONDS );
					break;

				default:
					break;
				}

				calculator.calc(metadata, startCalcDate, Math.min(System.currentTimeMillis(),
						metadata.getProjectDeActivationDate() + Constants.ONE_DAY_MILLISECONDS));
				break;
				
			case RECALCULATE_PROJECT_HOURLY:
				if (task.getDate()>System.currentTimeMillis()) {
					return true;
				}
				if (trigger == null) {
					recalcPlant = true;
					recalcWater = true;
					recalcFruit = true;
				} else {
					switch (trigger.getSensorKind()) {
					case PIVOT:
					case WATER_PRESSURE:
					case IRRIGATION:
					case SOIL_MOISTURE:
						recalcWater = true;
						break;
					case PLANT:
						recalcPlant = true;
						break;
					case FRUIT:
						recalcFruit = true;
						break;
					default:
						return true;
					}
				}

				// equal to soft project recalc, i.e only high level project calculation without
				// sensors recalc
				recalculateProjectHourlyData(metadata, task.getDate(), null, recalcPlant, recalcWater, recalcFruit);
				break;

			case FULL_RECALCULATE_PROJECT_HOURLY:
				if (task.getDate()>System.currentTimeMillis()) {
					return true;
				}
				// recalculate sensors and then project itself
				// if task.date == null it means we ask to recalc from the very beginning of the
				// project

				if (trigger == null) {
					recalcPlant = true;
					recalcWater = true;
					recalcFruit = true;
				} else {
					switch (trigger.getSensorKind()) {
					case PIVOT:
					case WATER_PRESSURE:
					case IRRIGATION:
					case SOIL_MOISTURE:
						recalcWater = true;
						break;
					case PLANT:
						recalcPlant = true;
						break;
					case FRUIT:
						recalcFruit = true;
						break;
					default:
						return true;
					}
				}

				// recalculate plant sensors - stitch
				if (recalcPlant) {
					new PlantSensorCalculator().recalculateAllSensors(metadata, task.getDate(),
							System.currentTimeMillis());
				}

				// "high level" project hourly calculations
				recalculateProjectHourlyData(metadata, task.getDate(), null, recalcPlant, recalcWater, recalcFruit);

				break;

			default:
				return false;
			}

		} catch (Exception e) {
			log.error("Error handling message", e);
			return false;
		}

		return true;

	}

	private void handeleRecalculateDailyIrrigation(Integer projectId, Long start, Long end) {
		WaterPresureHourlyCalculator calculator = new WaterPresureHourlyCalculator();
		ProjectMetadata metadata=PhytechApi.getProjectMetaData(projectId);

		DBQueriesManagerProjects.deleteIrrigationSpans(projectId, start);
		calculator.calc(metadata, start, System.currentTimeMillis());

		// calculator.recalculateDailyIrrigation(projectId, start, end);
	}

	private void recalculateProjectHourlyData(ProjectMetadata metadata, Long start, Long end, boolean recalcPlant,
			boolean recalcIrrigation, boolean recalcFruit) {

		if (recalcPlant) {
			new PlantCalculator().calc(metadata, start, end);
		}
		if (recalcFruit) {
			new FruitCalculator().calc(metadata, start, end);
		}
		if (recalcIrrigation) {
			// delete old spans
			DBQueriesManagerProjects.deleteIrrigationSpans(metadata.getProjectId(), start);
			if (metadata.isFlood()) {
				new SoilMoistureHourlyCalculator().calc(metadata, start, end);
			} else {
				new WaterPresureHourlyCalculator().calc(metadata, start, end);
			}
		}

	}

}
