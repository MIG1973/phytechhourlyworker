package hourlyWorker_runner;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.phytech.database.DBConnectionPoolManager;
import com.phytech.database.EnumDBType;
import com.phytech.utils.Constants;
import com.phytech.utils.PhytechApi;
import com.phytech.utils.PropertiesManager;
import com.phytech.utils.QueueSender;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;

public class HourlyWorker {
	private static final Logger log = Logger.getLogger(HourlyWorker.class);

	public static void main(String[] args) {
		log.info("System Configuration:");
		log.info("--------------------------------------------------------------------------------------");

		PropertiesManager.initConfiguration(System.getenv(Constants.CONFIGURATION_FILE_VARIABLE_NAME));

		log.info("--------------------------------------------------------------------------------------");
		DBConnectionPoolManager.createPoolForDB(EnumDBType.JAVA);

		if (!PropertiesManager.isDebug()) {
			PhytechApi.initAllProjectsMetadata();
		}
		QueueSender qSender = new QueueSender(PropertiesManager.hourlyTaskQueueName, QueueSender.QueueWithPriorityParams);

		ListenOnWorkerTaskQueue(qSender);

	}

	private static void ListenOnWorkerTaskQueue(QueueSender qSender) {
		try {

			ConnectionFactory factory = new ConnectionFactory();
			factory.setUri(PropertiesManager.rabbitMqHost);

			Connection connection = factory.newConnection();
			Channel channel = connection.createChannel();
			// limit the channel to wait for every single ACK
			channel.basicQos(1);
			Map<String, Object> args = new HashMap<String, Object>();
			args.put("x-max-priority", Constants.MAX_PRIORITY);
			channel.queueDeclare(PropertiesManager.hourlyTaskQueueName, true, false, false, args);
			Consumer consumer = new HourlyTaskQueueConsumer(channel, qSender);
			channel.basicConsume(PropertiesManager.hourlyTaskQueueName, false, "HOURLY_WORKER", consumer);

			log.info(" [*] Waiting for worker tasks.");

		} catch (Exception e) {
			log.error("ListenOnWorkerTaskQueue error opening queue", e);

		}
	}

}
